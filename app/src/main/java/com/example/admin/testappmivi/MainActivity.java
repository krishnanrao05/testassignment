package com.example.admin.testappmivi;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    TextView data_balance,credit_balance,roll_over_credit_balance,
             roll_over_data_balance,internation_talktime_balance,expiry_date,
             auto_renewal,primary_subscription,product_name,included_data,included_credit,
             included_international_talk,unlimited_text,unlimited_talk,unlimited_international_text,
             unlimited_international_talk_time,price ;

    JSONObject subscription_data,product_data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
      data_balance = (TextView)findViewById(R.id.data_balance);
      credit_balance = (TextView)findViewById(R.id.credit_balance);
      roll_over_credit_balance = (TextView) findViewById(R.id.roll_over_credit_balance);
      roll_over_data_balance = (TextView)findViewById(R.id.roll_over_data_balance);
      internation_talktime_balance = (TextView)findViewById(R.id.internation_talktime_balance);
      expiry_date = (TextView)findViewById(R.id.expiry_date);
      auto_renewal = (TextView)findViewById(R.id.auto_renewal);
      primary_subscription = (TextView) findViewById(R.id.primary_subscription);
      product_name = (TextView) findViewById(R.id.product_name);
      included_data = (TextView) findViewById(R.id.included_data);
      included_credit = (TextView) findViewById(R.id.included_credit);
      included_international_talk = (TextView)findViewById(R.id.included_international_talk);
      unlimited_text = (TextView) findViewById(R.id.unlimited_text);
      unlimited_talk = (TextView)findViewById(R.id.unlimited_talk);
      unlimited_international_text = (TextView) findViewById(R.id.unlimited_international_text);
      unlimited_international_talk_time = (TextView)findViewById(R.id.unlimited_international_talk_time);
      price = (TextView) findViewById(R.id.price);

      try{

      if(sharedpreferences.getString("details1",null) != null){
          Log.e("details1", "onCreate: "+sharedpreferences.getString("details1",null));
         subscription_data = new JSONObject(sharedpreferences.getString("details1",null));
         float balance = subscription_data.getJSONObject("attributes").getInt("included-data-balance") / 1000;
          Log.e("balance databalance", "onCreate: "+balance);
          data_balance.setText(String.valueOf(balance) +"GB");
          if(subscription_data.getJSONObject("attributes").getString("included-credit-balance").equalsIgnoreCase("null")){
              credit_balance.setText("Not Applicable");
          }
          else{
              credit_balance.setText(subscription_data.getJSONObject("attributes").getString("included-credit-balance"));
          }
          if(subscription_data.getJSONObject("attributes").getString("included-rollover-credit-balance").equalsIgnoreCase("null")){
              roll_over_credit_balance.setText("Not Applicable");
          }
          else{
              roll_over_credit_balance.setText(subscription_data.getJSONObject("attributes").getString("included-rollover-credit-balance"));
          }
          if(subscription_data.getJSONObject("attributes").getString("included-rollover-data-balance").equalsIgnoreCase("null")){
              roll_over_data_balance.setText("Not Applicable");
          }
          else{
              roll_over_data_balance.setText(subscription_data.getJSONObject("attributes").getString("included-rollover-data-balance"));
          }
          if(subscription_data.getJSONObject("attributes").getString("included-international-talk-balance").equalsIgnoreCase("null")){
              internation_talktime_balance.setText("Not Applicable");
          }
          else{
              internation_talktime_balance.setText(subscription_data.getJSONObject("attributes").getString("included-international-talk-balance"));
          }




          expiry_date.setText(subscription_data.getJSONObject("attributes").getString("expiry-date"));
          auto_renewal.setText(subscription_data.getJSONObject("attributes").getString("auto-renewal"));
          primary_subscription.setText(subscription_data.getJSONObject("attributes").getString("primary-subscription"));

      }if(sharedpreferences.getString("details2",null) != null){
          Log.e("details2", "onCreate: "+sharedpreferences.getString("details2",null));
          product_data = new JSONObject(sharedpreferences.getString("details2",null));
          product_name.setText(product_data.getJSONObject("attributes").getString("name"));

          if(product_data.getJSONObject("attributes").getString("included-data").equalsIgnoreCase("null")){
              included_data.setText("Not Applicable");
          }
          else {
              included_data.setText(product_data.getJSONObject("attributes").getString("included-data"));
          }
          if(product_data.getJSONObject("attributes").getString("included-credit").equalsIgnoreCase("null")){
                  included_credit.setText("Not Applicable");
          }
          else {
                  included_credit.setText(product_data.getJSONObject("attributes").getString("included-credit"));
          }
          if(product_data.getJSONObject("attributes").getString("included-international-talk").equalsIgnoreCase("null")){
              included_international_talk.setText("Not Applicable");
          }
          else {
              included_international_talk.setText(product_data.getJSONObject("attributes").getString("included-international-talk"));
          }


          unlimited_text.setText(product_data.getJSONObject("attributes").getString("unlimited-text"));
          unlimited_talk.setText(product_data.getJSONObject("attributes").getString("unlimited-talk"));
          unlimited_international_text.setText(product_data.getJSONObject("attributes").getString("unlimited-international-text"));
          unlimited_international_talk_time.setText(product_data.getJSONObject("attributes").getString("unlimited-international-talk"));
          price.setText(product_data.getJSONObject("attributes").getString("price") + "Rs");


      }

          }
          catch (JSONException e){
          e.printStackTrace();
          }


    }




}
