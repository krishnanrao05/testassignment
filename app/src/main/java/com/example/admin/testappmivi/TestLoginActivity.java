package com.example.admin.testappmivi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TestLoginActivity extends AppCompatActivity {


    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    JSONObject detail_data,detail1_data,detail2_data;

    TextView msnNumber;
    Button loginBtn;
    String msnCredential;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_login2);
       loginBtn = (Button) findViewById(R.id.loginBtn);
       msnNumber = (TextView) findViewById(R.id.msnNumber);
        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        try {
            JSONObject obj = new JSONObject(loadDataFromAsset());
            Log.e("Json String", "onCreate: "+obj.toString());
            JSONArray m_jArry = obj.getJSONArray("included");

            for (int i = 0; i < m_jArry.length(); i++) {

                editor.putString("details"+i,m_jArry.get(i).toString());
                Log.e("JSON Details:", "onCreate: "+m_jArry.get(i).toString());
            }
            editor.apply();
            detail_data = new JSONObject(sharedpreferences.getString("details0",null));
            detail1_data = new JSONObject(sharedpreferences.getString("details1",null));
            detail2_data = new JSONObject(sharedpreferences.getString("details2",null));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("detail_data: ", "onCreate: "+detail_data);
        Log.e("detail1_data: ", "onCreate: "+detail1_data);
        Log.e("detail2_data: ", "onCreate: "+detail2_data);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                msnCredential = msnNumber.getText().toString();
                if(!msnCredential.matches("")){
                    try {
                        Log.e("MSNNUMBER: ", "onClick: "+detail_data.getJSONObject("attributes").getString("msn"));

                        if(msnCredential.equalsIgnoreCase(detail_data.getJSONObject("attributes").getString("msn"))){
                            Intent nextActivity = new Intent(TestLoginActivity.this,SplashScreen.class);
                            startActivity(nextActivity);
                        }
                        else {
                            Toast.makeText(TestLoginActivity.this, "MSN Invalid !", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.e("MsnNumber Entered", "onClick: "+msnNumber.getText());
                }
                else{
                    Toast.makeText(TestLoginActivity.this, "Please Enter MSN Number: ", Toast.LENGTH_SHORT).show();
                }
            }
        });


 }

    public String loadDataFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("collection.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
